@echo off
setlocal enabledelayedexpansion
set SERVER=http-pierce

REM Check JAVA_HOME environment variable
if defined JAVA_HOME (
    echo JAVA_HOME is set to %JAVA_HOME%
    set "PATH=%JAVA_HOME%\bin;%PATH%"
    call:checkJavaVersion
) else (
    echo JAVA_HOME is not set. Checking system path.
    call:checkJavaVersion
)

REM Function to get Java version
:checkJavaVersion
    java -version 2>&1 | findstr /r /c:"version \"[1-9][7-9]"
    if %errorlevel% equ 0 (
      echo %errorlevel%
      call:runApp
    ) else (
        echo JDK 17 or above is not found.
        call:last
    )

REM 启动程序
:runApp
    set "JAVA_OPT= -Xms512m -Xmx512m -Xmn256m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m"
    echo %JAVA_OPT%

    call "%JAVA%" %JAVA_OPT% -jar %SERVER%.jar
    pause & EXIT

:last
REM If the script reaches this point, JDK 17+ was not found
echo Please install JDK 17 or above and ensure JAVA_HOME is set correctly.
pause & EXIT 