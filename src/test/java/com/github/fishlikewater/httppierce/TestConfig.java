package com.github.fishlikewater.httppierce;

import io.github.fishlikewater.raiden.config.ConfigParser;
import io.github.fishlikewater.raiden.config.ini.Ini;
import io.github.fishlikewater.raiden.core.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * {@code TestConfig}
 *
 * @author zhangxiang
 * @since 2025/02/12
 */
public class TestConfig {

    @Test
    public void test() throws IOException {
        ConfigParser configParser = new ConfigParser();
        // Example usage: Load a configuration file
        Ini ini = configParser.readIni(FileUtils.file("application.properties"));
        String string = ini.getString("spring.profiles.active");
        System.out.println(string);
    }

    @Test
    public void test2() throws IOException {
        Pattern PROPERTY = Pattern.compile("(^\\w[\\w.]*)\\s*=\\s*(.*?)");
        Matcher matcher = PROPERTY.matcher("server.type=1");
        while (matcher.find()) {
            System.out.println(matcher.group(1));
        }
    }
}
