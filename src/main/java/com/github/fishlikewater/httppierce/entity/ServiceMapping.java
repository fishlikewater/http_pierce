package com.github.fishlikewater.httppierce.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serial;

/**
 * 实体类。
 *
 * @author fishl
 * @since 2023-09-01
 */
@Data
@Accessors(chain = true)
@Table(value = "service_mapping")
@EqualsAndHashCode(callSuper = true)
public class ServiceMapping extends Model<ServiceMapping> {

    @Serial
    private static final long serialVersionUID = 6832032908680582387L;

    @Id(keyType = KeyType.Auto)
    private Integer id;

    private String name;

    private String address;

    private Integer localPort;

    private String registerName;

    private Integer delRegisterName;

    private Integer newServerPort;

    private Integer newPort;

    private String protocol;

    private Integer enable;

    @Column(ignore = true)
    private Integer state;

    @Column(ignore = true)
    private String remoteAddress;
}
