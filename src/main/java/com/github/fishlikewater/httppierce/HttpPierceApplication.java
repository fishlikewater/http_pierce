package com.github.fishlikewater.httppierce;

import com.github.fishlikewater.httppierce.config.Constant;
import io.github.fishlikewater.raiden.config.ConfigParser;
import io.github.fishlikewater.raiden.config.ini.Ini;
import io.github.fishlikewater.raiden.core.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.io.IOException;

/**
 * @author fishl
 */
@Slf4j
@SpringBootApplication
@EnableTransactionManagement
@MapperScan("com.github.fishlikewater.httppierce.mapper")
public class HttpPierceApplication {

    public static void main(String[] args) throws IOException {
        ConfigParser configParser = new ConfigParser();
        Ini ini = configParser.readIni(FileUtils.file("application.properties"));
        String active = ini.get("spring.profiles.active", String.class, Constant.SERVER);
        SpringApplication app = new SpringApplication(HttpPierceApplication.class);
        if (Constant.SERVER.equals(active)) {
            app.setWebApplicationType(WebApplicationType.NONE);
        }
        app.run(args);
    }
}
