package com.github.fishlikewater.httppierce.kit;

import io.github.fishlikewater.raiden.core.id.Snowflake;

/**
 * {@code IdUtil}
 * Id生成器
 *
 * @author zhangxiang
 * @date 2024/02/04
 * @since 1.0.1
 */
public class IdUtil {

    private static final Snowflake snowflake = new Snowflake();

    public static long generateId() {
        return snowflake.nextId();
    }
}
