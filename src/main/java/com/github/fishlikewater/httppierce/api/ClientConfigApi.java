package com.github.fishlikewater.httppierce.api;

import com.github.fishlikewater.httppierce.api.model.ServiceMappingRequest;
import com.github.fishlikewater.httppierce.entity.ServiceMapping;
import com.github.fishlikewater.httppierce.kit.ClientKit;
import com.github.fishlikewater.httppierce.service.ServiceMappingService;
import io.github.fishlikewater.raiden.core.model.Result;
import io.github.linpeilie.Converter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 客户端数据接口
 * </p>
 *
 * @author fishlikewater@126.com
 * @since 2023年09月01日 13:00
 **/
@RestController
@RequiredArgsConstructor
@RequestMapping("/client/api")
@ConditionalOnProperty(prefix = "spring.profiles", name = "active", havingValue = "client")
public class ClientConfigApi {

    private final ServiceMappingService serviceMappingService;
    private final Converter converter;

    @GetMapping
    public Result<List<ServiceMapping>> getList() {
        final List<ServiceMapping> list = serviceMappingService.querylist();
        return Result.of(list);
    }

    /**
     * 启用
     *
     * @param id id
     * @return {@link Result<String>}
     */
    @PutMapping("/{id}")
    public Result<Void> enable(@PathVariable("id") Integer id) {
        serviceMappingService.enable(id);
        return Result.ok();
    }

    @PostMapping
    public Result<Void> edit(@RequestBody @Validated ServiceMappingRequest request) {
        serviceMappingService.edit(converter.convert(request, ServiceMapping.class));
        return Result.ok();
    }

    @DeleteMapping("/{id}")
    public Result<Void> del(@PathVariable("id") Integer id) {
        serviceMappingService.delById(id);
        return Result.ok();
    }

    @PostMapping("/reboot")
    public Result<Void> reboot() {
        ClientKit.getClientBoot().stop();
        ClientKit.getClientBoot().start();
        return Result.ok();
    }
}

